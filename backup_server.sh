# -*- coding:utf-8 -*-
#!/bin/sh
date >> ~/backup_server.log
(time rsync -av -8 --delete --exclude=Thumbs.db --exclude=*.tmp --exclude=スキャナ/ --exclude=バックアップ/ /cygdrive/g/ /cygdrive/d/backup_server/) >> ~/backup_server.log 2>&1
echo "$n" >> ~/backup_server.log
