#!/bin/sh
DEST="/media/BACKUP"
LOG_FILE=${HOME}"/logfiles/backup_local.log"

# Backup org
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/org ${DEST}/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup documents
(time rsync -av -8 --delete --exclude-from exclude_files --chmod=u+rwx ${HOME}/Documents ${DEST}/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup inbox, contact_sheet mail mail
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/Mail/inbox ${HOME}/Mail/contact_sheet ${DEST}/Mail/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup src
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/src ${DEST}/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup .emacs.d
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/.emacs.d/* ${DEST}/dot_emacs/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup code
(time rsync -av -8 --delete --exclude-from exclude_files --exclude 'vendor' --exclude 'tmp' ${HOME}/code ${DEST}/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup .zsh
(time rsync -av -8 --delete --exclude-from exclude_files --exclude '.git' ${HOME}/.zsh/ ${DEST}/dot_zsh) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup .ssh
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/.ssh/ ${DEST}/dot_ssh) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup bin
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/bin ${DEST}/) >> ${LOG_FILE} 2>&1
echo "$n" >> ${LOG_FILE}

# Backup database of rails application
mysqldump -u root -pecohiruma payment_production > ${HOME}/rails/payment.dump
mysqldump -u root -pecohiruma personal_management_production > ${HOME}/rails/personal_management.dump

(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/rails/payment.dump ${HOME}/rails/payment/public/images/stored_files ${DEST}/payment/) >> ${LOG_FILE} 2>&1
(time rsync -av -8 --delete --exclude-from exclude_files ${HOME}/rails/personal_management.dump ${HOME}/rails/personal_management/public/images/stored_files ${DEST}/personal_management/) >> ${LOG_FILE} 2>&1

echo "$n" >> ${LOG_FILE}
