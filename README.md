# 会社用バックアップ #

## ファイル構成 ##

* README.md         このファイル
* backup_local.sh   ローカルのバックアップ
* backup_server.sh  ファイルサーバのバックアップ
* backup_all.sh     上記2つを一緒に起動
* exclude_files     共通的なバックアップの除外リスト
* Rakefile          インストール用rakeファイル

## 準備(インストール) ##

1. 「$HOME/bin/」にパスを通す

2. 「$HOME/bin/」というディレクトリを作成

3. 以下を起動

    rake install

## 起動方法 ##

ローカルファイルのみをバックアップ(バックアップ終了後、シャットダウンされる)

    backup_local.sh

サーバファイルのみをバックアップ

    backup_server.sh

両方バックアップ(バックアップ終了後、シャットダウンされる)

    backup_all.sh

## ログ ##

以下のファイルにログが保存される

* ローカルバックアップのログ

    $HOME/backup_local.log

* サーババックアップのログ

    $HOME/backup_server.log

